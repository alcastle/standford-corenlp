# standford-corenlp

This is exactly the same brew formula as the default stanford-corenlp. 
```
# This is the default
brew install stanford-corenlp
```

With the exception that it is using the latest release from Stanfords website 
[](https://stanfordnlp.github.io/CoreNLP/) and the sha256 hash has been updated.

The older official formula doesn't seem to work with my latest version of Java 
and OSX Mojave.

## To Use
```
 $ brew tap alcastle/stanford-corenlp https://gitlab.com/alcastle/standford-corenlp
 $ brew install alcastle/stanford-corenlp/stanford-corenlp
```
